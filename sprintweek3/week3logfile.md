## Log file week 3
6/12/19

| Task                     | Date     |Start Time| End Time | Comments |
| ------------------------ | -------- |----------|----------|----------|
| update rasp              |5/25/2019 | 12:22    | 12:32    |Created account ran a few scans|
| install dnsmasq           |5/25/2019 | 13:00    | 13:25    |search for hostname, find ip web server, internet services,OS|
| configure dnsmasq | 14:00    | on-going |site: filetype:pdf password |
| install hostapd |5/25/2019 | 14:35    | 14:40    |use command **keys add shodan_api** (had to look up how to do it) https://hackertarget.com/recon-ng-tutorial/ |
| determine Ip scheme        |5/25/2019 | 15:00    | on-going |show modules, show info, issue with running some commands using shodan api need to upgrade to PRO versiona|
| Turn on ssh |5/25/2019 | 15:00    | on-going |show modules, show info, issue with running some commands using shodan api need to upgrade to PRO versiona|
|Edit the dhcpd.conf file|5/25/2019 | 15:00    | on-going |show modules, show info, issue with running some commands using shodan api need to upgrade to PRO versiona
