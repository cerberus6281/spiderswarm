## enable ssh

![](https://gitlab.com/cerberus6281/spiderswarm/raw/master/snapss/sprintdelivs/sshenabled.PNG)  
## script that will install the necessary files
![](https://gitlab.com/cerberus6281/spiderswarm/raw/master/snapss/sprintdelivs/installfiles.PNG)
## create ip scheme

![](https://gitlab.com/cerberus6281/spiderswarm/raw/master/snapss/sprintdelivs/iprange.PNG)  

## create wifi router access point
![](https://gitlab.com/cerberus6281/spiderswarm/raw/master/snapss/sprintdelivs/interfaceseteup.PNG)  

## create the interface dhcp range
![](https://gitlab.com/cerberus6281/spiderswarm/raw/master/snapss/sprintdelivs/iponfiguartion.PNG)  

## configure network

### create the daemon and set the file path
![](https://gitlab.com/cerberus6281/spiderswarm/raw/master/snapss/sprintdelivs/addtheline.PNG)  

### settings for router
![](https://gitlab.com/cerberus6281/spiderswarm/raw/master/snapss/sprintdelivs/routersettings.PNG)  

## Test network connections
![](https://gitlab.com/cerberus6281/spiderswarm/raw/master/snapss/sprintdelivs/succesfulping.PNG)  

## your wifiwill now find the spiderswarm network 

![](https://gitlab.com/cerberus6281/spiderswarm/raw/master/snapss/sprintdelivs/tests.PNG)  
# gitlab
![](https://gitlab.com/cerberus6281/spiderswarm/raw/master/snapss/sprintdelivs/gitlab.PNG)